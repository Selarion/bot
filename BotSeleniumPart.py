# -*- coding: utf-8 -*-

"""
    Модуль логина в стим:
    На вход подается открытый webdriver и логин/пароль от аккаунта.
    Открыть второй вебдрайвер -> Открыть в нем мыло -> Открыть в нем письмо и
    вытащить код -> вставить код в стим -> Проверяем, что мы залогинены и на выходе отдаем залогиненый драйвер.
"""

import time
import logging

from selenium import webdriver
from selenium.common.exceptions import *
from selenium.webdriver.common.keys import Keys

import AdditionalMethods





def TakeCodeFromMail(LoginMail, PassMail):
    while 1:
        try:
            driver2 = webdriver.Firefox()
            driver2.implicitly_wait(5)

            #Login in mail
            driver2.get("http://mail.ru")
            driver2.find_element_by_xpath(".//*[@id='mailbox__login']").send_keys(LoginMail)
            driver2.find_element_by_xpath(".//*[@id='mailbox__password']").send_keys(PassMail + Keys.ENTER)
            time.sleep(8)

            #Perehodim po ssulke, vudernutoi s pomosch'u metoda "get_attribute" prislannui kod
            element = driver2.find_element_by_xpath(".//*[@id='b-letters']/div[2]/div[1]/div/div[2]/div[1]/div/a")
            driver2.get(element.get_attribute('href'))
            code = driver2.find_element_by_xpath(".//*[starts-with(@id, 'style_')]/div/h2").text
            driver2.quit()
            return code

        except(NoSuchElementException):
            driver2.quit()
            continue


def CheckLoginSteam(driver):
    print ("Login check..."),
    try:
        driver.find_element_by_class_name("user_info")
        print ("successfully")
        return True
    except:
        print ("not completed")
        return False


def TryLogin(driver, WorkPageUrl, LoginSteam, PassSteam, LoginMail, PassMail):
    counter = 0
    while 1:
        try:
            tit1 = time.time()

            #Login in steam
            driver.get("https://steamcommunity.com/login/home/?goto=0")
            driver.find_element_by_xpath(".//*[@id='steamAccountName']").send_keys(LoginSteam)
            driver.find_element_by_xpath(".//*[@id='steamPassword']").send_keys(PassSteam + Keys.ENTER)

            time.sleep(8)
            code = AdditionalMethods.TakeCodeFromMail(LoginMail, PassMail)

            #Vbivaem kod i loginimsya
            driver.find_element_by_xpath(".//*[@id='authcode']").send_keys(code)
            driver.find_element_by_xpath(".//*[@id='friendlyname']").send_keys(3*code)
            driver.find_element_by_class_name("auth_button").click()

            time.sleep(4)
            tit2 = time.time()
            print ("Login time: ", tit2-tit1)
            driver.get(WorkPageUrl)
        except():
            counter += 1
            print ("Login error... %s/3" % counter)

        if CheckLoginSteam(driver) is True:
            break


def Login(driver, WorkPageUrl, LoginSteam, PassSteam, LoginMail, PassMail):
    if CheckLoginSteam(driver) is False:
        TryLogin(driver, WorkPageUrl, LoginSteam, PassSteam, LoginMail, PassMail)


def DriverRefresh(driver, WorkPageUrl, LoginSteam, PassSteam, LoginMail, PassMail):
    driver.close()
    time.sleep(3)
    driver = webdriver.Firefox()
    driver.implicitly_wait(4)
    driver.get(WorkPageUrl)
    Login(driver, WorkPageUrl, LoginSteam, PassSteam, LoginMail, PassMail)
    return driver
