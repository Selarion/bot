# -*- coding: utf-8 -*-

import threading
import time
import logging
import json

from grab import Grab
from grab import error

import AdditionalMethods
import BotGrabPart


def main(login_steam, pass_steam, login_mail, pass_mail):
    g = Grab()
    (g, steam_id) = BotGrabPart.LoginGrab(g, login_steam, pass_steam, login_mail, pass_mail)
    g.go('https://steamcommunity.com/market/')    

f = open('doc/LoginPass_all_account.txt')

for line in f:
    list_from_line = line.split(" ")
    list_from_line[-1] = list_from_line[-1][:-1]
    main(list_from_line[2], list_from_line[3], list_from_line[0], list_from_line[1])

f.close()
raw_input('The end')
