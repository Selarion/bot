# -*- coding: utf-8 -*-

import time
import json
import random

from grab import Grab
from grab import error

import AdditionalMethods
import BotGrabPart



def main(login_steam, pass_steam, login_mail, pass_mail):
    weapon_dictionary = {}
    stop = False
    cash = 0.0
    can = 0
    not_can = 0
    refresh_grab_counter = 0
    checker_market_page = 0

    print "------ \nPls, input game_number:\n0 - Counter-Strike:GO (appid730)\n1 - Team Fortress 2 (appid440)" \
    "\n2 - Dota2 (appid570) Rare\n3 - Dota2 (appid570) Uncommon"
    game_number = raw_input()
    game_appid_dict={'0': '730',
                     '1': '440',
                     '2': '570',
                     '3': '570'}
    game_appid = game_appid_dict[game_number]

    print "------\nPls input number hundred of first item in steam list... "
    num = raw_input()
    number_of_first_item = str(int(num)*100)
    number_of_item = 100

    #Cs:Go
    if game_number == '0':
        work_page = 'http://steamcommunity.com/market/search/render/?' \
                     'query=&start=%s&count=%s' % (number_of_first_item, number_of_item) + \
                     '&search_descriptions=0&sort_column=quantity&sort_dir=desc&appid=730&category_730_ItemSet%5B%5D=any' \
                     '&category_730_TournamentTeam%5B%5D=any&category_730_Weapon%5B%5D=any&' \
                     'category_730_Type%5B%5D=tag_CSGO_Type_SMG&category_730_Type%5B%5D=tag_CSGO_Type_Pistol&category_730_Type%5B%5D=tag_CSGO_Type_SniperRifle&category_730_Type%5B%5D=tag_CSGO_Type_Rifle&category_730_Type%5B%5D=tag_CSGO_Type_Shotgun&category_730_Type%5B%5D=tag_CSGO_Type_Machinegun?l=russian'

    #Team Fortress 2
    elif game_number == '1':
        work_page = 'http://steamcommunity.com/market/search/render/?' \
                    'query=&start=%s&count=%s' % (number_of_first_item, number_of_item) + \
                    '&search_descriptions=0&sort_column=quantity&sort_dir=desc&appid=440&category_440_Type%5B%5D=any&category_440_Class%5B%5D=tag_Pyro&category_440_Class%5B%5D=tag_Soldier&category_440_Class%5B%5D=tag_Sniper&category_440_Class%5B%5D=tag_Heavy&category_440_Class%5B%5D=tag_Demoman&category_440_Class%5B%5D=tag_Scout&category_440_Class%5B%5D=tag_Medic&category_440_Class%5B%5D=tag_Spy&category_440_Class%5B%5D=tag_Engineer?l=russian'
    #Dota2 Rare
    elif game_number == '2':
        work_page = 'http://steamcommunity.com/market/search/render/?' \
                    'query=&start=%s&count=%s' % (number_of_first_item, number_of_item) + \
                    '&search_descriptions=0&sort_column=quantity&sort_dir=desc&appid=570&category_570_Hero%5B%5D=any&category_570_Slot%5B%5D=any&category_570_Rarity%5B%5D=tag_Rarity_Rare'
    #Dota2 Uncommon
    elif game_number == '3':
        work_page = 'http://steamcommunity.com/market/search/render/?' \
                    'query=&start=%s&count=%s' % (str(int(number_of_first_item)+300), number_of_item) + \
                    '&search_descriptions=0&sort_column=quantity&sort_dir=desc&appid=570&category_570_Hero%5B%5D=any&category_570_Slot%5B%5D=any&category_570_Rarity%5B%5D=tag_Rarity_Uncommon&category_570_Type%5B%5D=tag_wearable'

    else:
        raw_input('Something wrong! Incorrect game_number')
        return None

    percent = float(raw_input('Percent (0.8, for example)'))

    g = Grab()
    (g, steam_id) = BotGrabPart.LoginGrab(g, login_steam, pass_steam, login_mail, pass_mail)
    log_file = 'log/%s-%s.html' % (login_steam, random.randint(10000, 99999))

    #Главный бесконечный цикл
    while True:
        g.setup(log_file=log_file)
        try:
            g.go('http://steamcommunity.com/market/')
            wallet = g.doc.select(".//*[@id='marketWalletBalanceAmount']").text()
        except:
            checker_market_page += 1
            if checker_market_page < 50:
                print "response not given"
                continue
            else:
                (g, steam_id) = BotGrabPart.RefreshGrabObject(login_steam, pass_steam, login_mail, pass_mail)
                checker_market_page = 0
                continue
        for i in range(60):
            tm = time.time()
            g.setup(url=work_page)
            try:
                g.request()
                response_body = json.loads(g.response.body)
                g1 = Grab(response_body['results_html'])
            except(error.GrabTimeoutError, ValueError, TypeError):
                time.sleep(0.5)
                continue
            except(error.GrabNetworkError, error.GrabConnectionError):
                time.sleep(10)
                break

            for line_number in range(number_of_item):
                #Проверка на прогрузку+выдергивание строки с оружием
                try:
                    weapon = g1.doc.select(".//*[@id='result_%s']" % line_number).text()
                except(error.DataNotFound):
                    print "Error loading! "
                    time.sleep(0.5)
                    break

                #Обработка строки с оружием
                weapon_info = weapon.split(' ')
                weapon_name = AdditionalMethods.ParseWeaponName(weapon_info, game_appid)
                try:
                    weapon_price = AdditionalMethods.UnicodeToFloat(weapon_info[1])
                except(ValueError):
                    print "Слетели куки"
                    (g, steam_id) = BotGrabPart.RefreshGrabObject(login_steam, pass_steam, login_mail, pass_mail)
                    break


                if (weapon_name in weapon_dictionary) is False:
                    print "Эта вещь добавлена в список"
                    weapon_dictionary[weapon_name] = weapon_price
                    continue

                try:
                    profit = round((((weapon_dictionary[weapon_name]-0.01)*percent)-weapon_price), 2)
                    print weapon_name
                    print "Старая цена - %s, Текущая цена - %s, Profit - %s" % (weapon_dictionary[weapon_name], weapon_price, profit)
                    weapon_dictionary[weapon_name] = weapon_price

                    if (profit > 0.05) and (weapon_price < 100.0):
                        print 'Ты поймал этот момент!'
                        link = g1.doc.select(".//*[@id='resultlink_"+str(line_number)+"']").attr('href')
                        checker = BotGrabPart.BuyAction(g, link, steam_id, game_appid, percent)
                        if checker is True:
                            can += 1
                            cash += profit
                        if checker == None:
                            raw_input('Момент истины, ибо перезапуск...')
                            (g, steam_id) = BotGrabPart.RefreshGrabObject(login_steam, pass_steam, login_mail, pass_mail)
                        not_can += 1
                except():
                    print ("Ошибка в вычисление профита")
                    raw_input()
            if stop is True:
                break
            time.sleep(0.05)
            print "- - - - - -"
            print time.time() - tm," sec per loop"
            print 'cash = %s' % cash
            print '%s wallet = %s' % (login_steam, wallet)
            print "%s / %s" % (can, not_can)
            print "-----------"
        #Перезагрузка бота для обновления куков
        refresh_grab_counter += 1
        if refresh_grab_counter > 1200:
            (g, steam_id) = BotGrabPart.RefreshGrabObject(login_steam, pass_steam, login_mail, pass_mail)
            refresh_grab_counter = 0
            try:
                while 1:
                    checker = BotGrabPart.SellItem(g, steam_id, game_appid)
                    print checker
                    if checker == False:
                        break
                    else:
                        i -= 1
            except():
                pass
if __name__ == '__main__':
    while 1:
        (checker, login_steam, pass_steam, login_mail, pass_mail) = AdditionalMethods.TakeLoginPassFromFile()
        if checker == True:
            main(login_steam, pass_steam, login_mail, pass_mail)
            raw_input('We were thrown from Steam O__O?!')