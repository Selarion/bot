# -*- coding: utf-8 -*-

import time
import json
import random

from grab import Grab
from grab import error

import AdditionalMethods
import BotGrabPart

def SellAll():
    print "------ \nPls, input game_number:\n0 - Counter-Strike:GO (appid730)\n1 - Team Fortress 2 (appid440)\n2 - Dota2 (appid570)"
    game_number = raw_input()
    game_appid_dict={'0': '730',
                     '1': '440',
                     '2': '570'}
    game_appid = game_appid_dict[game_number]
    begin = int(raw_input("Nomer pervogo accounta?\n"))
    end = int(raw_input("Nomer poslednego accounta?\n"))
    for i in range(begin, end+1):
        (checker, login_steam, pass_steam, login_mail, pass_mail) = AdditionalMethods.TakeLoginPassFromFile(i)
        g = Grab()
        g.setup(log_file='log/Sellall-%s.html' % (login_steam))
        (g, steam_id) = BotGrabPart.LoginGrab(g, login_steam, pass_steam, login_mail, pass_mail)
        checker = BotGrabPart.RemoveAllLots(g)
#        checker = True
        if checker == True:
            while 1:
                checker = BotGrabPart.SellItem(g, steam_id, game_appid)
                print checker
                if checker == False:
                    break
                else:
                    i -= 1
        print("All item sent")

if __name__ == '__main__':
    SellAll()
