# -*- coding: utf-8 -*-
import poplib


def UnicodeToFloat(price):
    """Преобразует цену-строку в вещественное число"""
    if price == u'Продано!':
        print 'Продано! =('
    if (price.find(',') != -1):
        price = price.replace(",", ".")
        price = float(price)
        return price
    else:
        price = float(price)
        return price


def ParseWeaponName(weapon_info, game_appid):
    j = 0
    if (game_appid == '730') or (game_appid == '440'):
        j = 3
    elif game_appid == '570':
        j = 2
    if j == 0:
        raw_input('Error. ParseWeaponName. j = 0. Не сработал костыль по обработке имени.')
    weapon_name = ''
    for i in range(4, len(weapon_info)-j):
        weapon_name = weapon_name + weapon_info[i]+' '
    weapon_name = weapon_name.rstrip().encode('utf-8') #удаляем лишний пробел, получившийся при составление строки
    return weapon_name


def TakeLoginPassFromFile(number_of_account=None):

    f = open('doc/LoginPass_all_account.txt')
    list_accounts = f.readlines()
    for i in range(len(list_accounts)):
        list_accounts[i] = list_accounts[i][:-1]
        print '%s - %s' % (i, list_accounts[i])
    if number_of_account == None:
        print '--'
        i = int(raw_input("Input bot account number, please. Now u have %s accounts.\n" % len(list_accounts)))
    else:
        i = number_of_account
    if i > (len(list_accounts)-1):
        print "Error. Maybe account with this NUMBER(!) is not in this list."
        return (False, None, None, None, None)
    a = list_accounts[i].split(" ")
    login_mail = a[0]
    pass_mail = a[1]
    login_steam = a[2]
    pass_steam = a[3]
    print a

    f.close()
    if a != []:
        return (True, login_steam, pass_steam, login_mail, pass_mail)
    else:
        print 'Accaount with u number not found'
        return (False, None, None, None, None)


def TakeCodeFromMail(login_mail, pass_mail):
    i = ""
    try:
        server = poplib.POP3_SSL("pop.mail.ru")
        server.user(login_mail)
        server.pass_(pass_mail)

        number_last_mail = len(server.list()[1]) # количество писем в ящике, которое = номеру последнего письма.
        mail = server.retr(number_last_mail)[1] # само письмо
        for i in mail:
            if len(i) == 5:
                code = i
                break
        server.quit()
        if len(i) != 5:
            return False
        return code
    except:
        print "Error with TakeCodeFromMail"
        return TakeCodeFromMail(login_mail, pass_mail)

if __name__ == "__main__":
    print TakeCodeFromMail("nicholas-hoult@mail.ru", '7jdurpH8')