# -*- coding: utf-8 -*-
from grab import Grab
import time
import logging
import json
import base64
import random

import rsa
from grab import error

from AdditionalMethods import TakeCodeFromMail
from AdditionalMethods import UnicodeToFloat

logger = logging.getLogger('grab')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)

def LoginGrab(g, login_steam, pass_steam, login_mail, pass_mail):
    """Авторизация grab в стиме"""

    head_request = {'Host': 'steamcommunity.com',
                   'User-Agent': '"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0"',
                   'Accept': 'text/javascript, text/html, application/xml, text/xml, */*',
                   'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                   'Accept-Encoding': 'gzip, deflate',
                   'X-Requested-With': 'XMLHttpRequest',
                   'X-Prototype-Version': '1.7',
                   'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                   'Referer': 'https://steamcommunity.com/login/home/?goto=0',
                   'Connection': 'keep-alive',
                   'Pragma': 'no-cache',
                   'Cache-Control': 'no-cache'}

    body_request = {u'username': login_steam,
                   u'donotcache': str(int(time.time()*1000))}
    try:
        g.setup(headers=head_request, debug_post=True)

        #Запрос на получение модуля и экспоненты для составление Public ключа
        g.setup(url='https://steamcommunity.com/login/getrsakey/', post=body_request)
        g.request()
        str_response_body = g.response.body
        response_body = json.loads(str_response_body)

        #хэширование пароля
        mod = long(response_body['publickey_mod'], 16)
        exp = long(response_body['publickey_exp'], 16)
        pubKey = rsa.PublicKey(mod, exp)
        crypto = rsa.encrypt(pass_steam, pubKey)
        enc_password = base64.b64encode(crypto)

        #Логин ч.1 (Получение кода)
        update = {'username': login_steam,
                  "password": enc_password,
                  "emailauth": "",
                  "twofactorcod": "",
                  "loginfriendlyname": "",
                  "captchagid": "-1",
                  "captcha_text": "",
                  "emailsteamid": "",
                  "rsatimestamp": response_body["timestamp"],
                  "remember_login": 'False',
                  "donotcache": str(int(time.time()*1000))}

        body_request = update
        g.setup(url='https://steamcommunity.com/login/dologin/', post=body_request)
        g.request()
        str_response_body = g.response.body
        response_body = json.loads(str_response_body)
        print response_body['message'] + " ",

    #   НЕДОПИСАННАЯ ОБРАБОТКА КАПЧИ
        if response_body['message'] == u"Ошибка проверки человечности":
            print "Time.sleep(1200)"
            time.sleep(1200)
            return LoginGrab(g, login_steam, pass_steam, login_mail, pass_mail)
     #       print g.doc.select(".//*[@id='captchaImg']").attr('src')
      #      raw_input('Капча, пожалуйста: ')

        #Логин ч.2 (Дологин)
        time.sleep(6) #Время, чтобы дошло письмо.
        code = TakeCodeFromMail(login_mail, pass_mail)
        if code is False:
            print "No mail found?"
            return LoginGrab(g, login_steam, pass_steam, login_mail, pass_mail)
        print code
        update = {"emailauth": code,
                  "loginfriendlyname": 3*code,
                  'emailsteamid': '76561198118901890'}

        body_request.update(update)
        g.setup(url='https://steamcommunity.com/login/dologin/', post=body_request)
        g.request()
        str_response_body = g.response.body
        response_body = json.loads(str_response_body)

        if response_body['success'] is True:
            steam_id = response_body['transfer_parameters']['steamid']
            print 'Login success'
            g.setup(url='http://steamcommunity.com/actions/RedirectToHome')
            g.request()
            return g, steam_id
        else:
            print "Ошибка логина. Рекурсивно повторяю..."
            return LoginGrab(g, login_steam, pass_steam, login_mail, pass_mail)
    except:
        print u"Надо полагать, ошибка 15 секунд..."
        return LoginGrab(g, login_steam, pass_steam, login_mail, pass_mail)


def RefreshGrabObject(login_steam, pass_steam, login_mail, pass_mail):
    while True:
        try:
            print "refreshing grab object"
            g = Grab()
            print '--0'
            (g, steam_id) = LoginGrab(g, login_steam, pass_steam, login_mail, pass_mail)
            print '--1'
            g.setup(log_file='log/%s-%s.html' % (login_steam, random.randint(10000, 99999)))
            print '--2'
            return (g, steam_id)
        except:
            RefreshGrabObject(login_steam, pass_steam, login_mail, pass_mail)


def BuyAction(g, link, steam_id, game_appid, percent):
    try:
        g.go(link)
        first_price = UnicodeToFloat(g.doc.select(".//*[starts-with(@id, 'listing_')]/div[3]/span/span[1]")[0].text()[:-5])
        second_price = UnicodeToFloat(g.doc.select(".//*[starts-with(@id, 'listing_')]/div[3]/span/span[1]")[1].text()[:-5])
        item_name = g.doc.select(".//*[@id='mainContents']/div[1]/div/a[2]").text()
    except:
        return False
    print first_price
    print second_price

    if (first_price > 0) and (second_price > 0) is False:
        return False

    profit = (((second_price-0.01)*percent)-first_price)
    if profit > 0.05:
        checker = BuyItem(g)
        if checker is True:
            print "++++++++"
            checker = SellItem(g, steam_id, game_appid, item_name, second_price)
            if checker is True:
                print 'Sell done!'
                return True
        if checker == None:
            return None
    return False


def BuyItem(g):

    head_request = {'Host': 'steamcommunity.com',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0',
                    'Accept': '*/*',
                    'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                    'Accept-Encoding': 'gzip, deflate',
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Referer': 'http://steamcommunity.com/market/',
                    'Content-Lenght': '61',
                    'Origin': 'http://steamcommunity.com',
                    'Connection': 'keep-alive',
                    'Pragma': 'no-cache',
                    'Cache-Control': 'no-cache'}

    listing_id = g.doc.select(".//*[starts-with(@id, 'listing_')]/div[5]/span[1]")[0].attr('id')[8:-5]
    url = 'https://steamcommunity.com/market/buylisting/'+listing_id

    total_price = UnicodeToFloat(g.doc.select(".//*[starts-with(@id, 'listing_')]/div[3]/span/span[1]")[0].text()[:-5])
    subtotal_price = UnicodeToFloat(g.doc.select(".//*[starts-with(@id, 'listing_')]/div[3]/span/span[2]")[0].text()[:-5])
    fee = total_price - subtotal_price
    sessionid = g.response.cookies['sessionid']
    sessionid = sessionid.replace('%3D', '=')

    body_request = {'currency': '5',
                    'sessionid': sessionid,
                    'subtotal': str(int(subtotal_price*100)),
                    'fee': str(int(fee*100)),
                    'total': str(int(total_price*100)),
                    'quantity': '1'}

    g.setup(url=url, headers=head_request, post=body_request)
    while True:
        try:
            g.request()
            str_response_body = g.response.body
            response_body = json.loads(str_response_body)
        except:
            "Error. Buy_item not succsess. 'Buy_item' part error"
            return False

        try:
            print response_body['wallet_info']['success']
            print "Удалось купить."
            return True
        except:
            if response_body == []:
                print "response_body == []. Mesto, gde obrabativautsya oshibki pri pokupke"
                return False
            try:
                print response_body['message']
                if response_body['message'] == u'Ошибка при покупке предмета. Возможно, лот был удален. Обновите страницу и повторите попытку.':
                    return False
                if response_body['message'] == u'Не удалось купить данный предмет, так как его уже приобрел кто-то другой.':
                    return False
                if response_body['message'] == u'Не удалось связаться с сервером предметов данной игры. Возможно, сервер не работает, или с сетью Steam возникли временные проблемы. Обмен предметами и средствами, имеющимися в кошельке, не произошел. Обновите страницу и повторите попытку.':
                    return False
                if response_body['message'] == u'Кажется, в вашем браузере отключены cookie. Удостоверьтесь, что в вашем браузере разрешены все cookie, или попробуйте выйти, а затем снова войти в свой аккаунт.':
                    return None
                if response_body['message'] == u'Вы не можете сделать покупку, пока ваша предыдущая операция не завершится.':
                    time.sleep(1)
                    continue
                return False
            except:
                if response_body['wallet_info'] == 'false':
                    print "wallet_info == false"
                    return False


def SellItem(g, steam_id, game_appid='730', item_name=None, sell_price=None, flag=False,):
    """
    Если на вход оба аргумента, то все как обычно.
    Если только цена, то продается рандомная вещь.
    Если только имя, то продается по актуальной цене.
    """

    game_appid_dict = {'730': '730/2/',
                       '440': '440/2/',
                       '570': '570/2'}

    pos = 1
    checker = False
    item_hash_name = ''

    try:
        print 'Try request from invemtory and give json dictionary'
        g.go(u'http://steamcommunity.com/profiles/%s/inventory/' % steam_id)
        g.setup(url=u'http://steamcommunity.com/profiles/%s/inventory/json/%s' % (steam_id, game_appid_dict[game_appid]))
        g.request()
        response = json.loads(g.response.body)
    except:
        print "Inventory. Response not given"
        return SellItem(g, steam_id, game_appid=game_appid, item_name=item_name, sell_price=sell_price)

    try:
        if type(response['rgDescriptions']) == list:
            print 'No item found in inventory.'
            return False
    except: return False

    id_rgDescriptions = response['rgDescriptions'].keys()
    if id_rgDescriptions == []:
        print 'No item found in inventory. Conection error. id_rgDescriptions == [] raw_input.'
        raw_input()
        return False

    if item_name == None:
        rgInventory = response['rgInventory'].keys()
        while True:
            for i in range(len(rgInventory)):
                if response['rgInventory'][rgInventory[i]]['pos'] == pos:
                    class_id = str(response['rgInventory'][rgInventory[i]]['classid'])
                    instanceid = str(response['rgInventory'][rgInventory[i]]['instanceid'])
                    id = str(response['rgInventory'][rgInventory[i]]['id'])
                    checker = True
                    break
            if checker == False:
                print "Кажется, в Вашем инвентаре больше нет вещей, которые можно продать."
                return False
            key_Description = '%s_%s' % (class_id, instanceid)
            print "Pos = %s" % pos
            if response['rgDescriptions'][key_Description]['tradable'] == 0:
                pos += 1
                checker = False
                continue
            else:
                break

        item_name = response['rgDescriptions'][key_Description]['market_name']
        item_hash_name = response['rgDescriptions'][key_Description]['market_hash_name']
        print "Продаю рандомную шмотку! ",

    else:
        for item in id_rgDescriptions:
            if response['rgDescriptions'][item]['market_name'] == item_name:
                classid = response['rgDescriptions'][item]['classid']
                item_hash_name = response['rgDescriptions'][item]['market_hash_name']
                break

        if item_hash_name == "":
            for item in id_rgDescriptions:
                print response['rgDescriptions'][item]['market_name']
                print '----|||||-----'
            print 'No match between buy-item-name and all items in inventory. raw_input. ' \
                  'Now SellItem will be start again. time.sleep(7)'
            if flag is True:
                print '...but 1 one try has already been. So return True'
                return True
            time.sleep(7)
            return SellItem(g, steam_id, game_appid=game_appid, item_name=item_name, sell_price=sell_price, flag=True)

        id_rgInventory = response['rgInventory'].keys()
        for item in id_rgInventory:
            if response['rgInventory'][item]['classid'] == classid:
                id = response['rgInventory'][item]['id']
                break

    print "item_name  = %s" % item_name
    item_hash_name = item_hash_name.replace('|', '%7C')
    item_hash_name = item_hash_name.replace(' ', '%20')
    url = 'http://steamcommunity.com/market/priceoverview/?country=RU&currency=5&appid=%s' \
          '&market_hash_name=%s' % (game_appid, item_hash_name)
    g.setup(url=url)
    try:
        g.request()
        response = json.loads(g.response.body)
    except(error.GrabTimeoutError, ValueError):
        return SellItem(g, steam_id, item_name=item_name, sell_price=sell_price)
    if response['success'] == True:
        try:
            lowest_price = response['lowest_price']
        except:
            print 'скользкое место'
            return SellItem(g, steam_id, game_appid=game_appid, item_name=item_name, sell_price=sell_price)
    else:
        if flag is True:
                print 'flag == True. In place запрос на получение вещи прошел неудачно.'
                return True
        print 'Запрос на получение цены вещи из инвентаря прошел не удачно. raw_input'
        time.sleep(7)
        return SellItem(g, steam_id, game_appid=game_appid, item_name=item_name, sell_price=sell_price, flag=True)

    if sell_price == None:
        lowest_price = lowest_price[:-17]
        lowest_price = UnicodeToFloat(lowest_price)
        price = round(lowest_price/1.15, 2)-0.01
        print 'Цена продажи c вычетом комиссии - %s, без вычета комисии - %s' % (price, lowest_price)
    else:
        price = round(sell_price/1.15, 2)-0.01
        print 'Цена продажи c вычетом комиссии - %s, без вычета комисии - %s' % (price, sell_price)

    sessionid = g.response.cookies['sessionid']
    sessionid = sessionid.replace('%3D', '=')
    url = 'https://steamcommunity.com/market/sellitem/'
    head_request = {'Host': 'steamcommunity.com',
                    'User-Agent': '"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0"',
                    'Accept': 'text/javascript, text/html, application/xml, text/xml, */*',
                    'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                    'Accept-Encoding': 'gzip, deflate',
                    'X-Requested-With': 'XMLHttpRequest',
                    'X-Prototype-Version': '1.7',
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Referer': 'http://steamcommunity.com/profiles/%s/inventory/' % steam_id,
                    'Connection': 'keep-alive',
                    'Pragma': 'no-cache',
                    'Cache-Control': 'no-cache'}

    body_request = {'sessionid': sessionid,
                    'appid': game_appid,
                    'contextid': '2',
                    'assetid': id,
                    'amount': '1',
                    'price': str(int(price*100))}

    g.setup(url=url, headers=head_request, post=body_request)
    try:
        g.request()
        return True
    except:
        print "Inventory. Response not given. 2"
        return SellItem(g, steam_id, item_name=item_name, sell_price=sell_price, flag=True)


def RemoveAllLots(g):
    g.setup(log_file='log/out.html')
    cont = 0
    print 'RemoveAllLots mod ON'
    sessionid = g.response.cookies['sessionid']
    sessionid = sessionid.replace('%3D', '=')
    body_request = {'sessionid': sessionid}

    head_request = {'Host': 'steamcommunity.com',
                    'User-Agent': '"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0"',
                    'Accept': 'text/javascript, text/html, application/xml, text/xml, */*',
                    'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                    'Accept-Encoding': 'gzip, deflate',
                    'X-Requested-With': 'XMLHttpRequest',
                    'X-Prototype-Version': '1.7',
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Referer': 'http://steamcommunity.com/market',
                    'Connection': 'keep-alive',
                    'Pragma': 'no-cache',
                    'Cache-Control': 'no-cache'}
    while True:
        try:
            g.go('http://steamcommunity.com/market/')
            remove_item_list = g.doc.select(".//*[starts-with(@class, 'market_listing_row market_recent_listing_row listing_')]").attr_list('id')
            break
        except:
            print "EBOLA! SPASAISYA! [1]"
            cont += 1
            print cont
            if cont > 20:
                return False
            continue
    print remove_item_list
    g.setup(headers=head_request)

    for id in range(len(remove_item_list)):
        try:
            print remove_item_list[id][10:]
            url = 'http://steamcommunity.com/market/removelisting/%s' % remove_item_list[id][10:]
            g.setup(url=url, post=body_request)
            g.request()
        except:
            print "EBOLA! SPASAISYA! [2]"
            raw_input()
            return False
        time.sleep(0.5)

    try:
        g.go('http://steamcommunity.com/market/')
        remove_item_list = g.doc.select(".//*[starts-with(@class, 'market_listing_row market_recent_listing_row listing_')]").attr_list('id')
        if remove_item_list != []:
            return RemoveAllLots(g)
    except:
        print "EBOLA! SPASAISYA! [3]"
        raw_input()
        return False

    print('The end of remove all lots. raw_input')
    return True
